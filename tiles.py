import libtcodpy as T
from globals import *
import util

class Tile(object):

    #colors is five triplets, e.g.:
    #array(('fg' => [r,g,b], 
    #       'bg'=>[r,g,b], 
    #       'dark_fg' => [r,g,b]
    #       'dark_bg' => [r,g,b]
    #       'glow' => [r_precent,g_percent,b_percent]
    #)
    
    # ^^^ this all had bullshit looks, use colordance instead

    colordance = False
    block_pass = True
    block_gas = block_pass #modify in child class if you need
    block_sight = True
    explored = False
    must_draw = False
    is_safe = True
    
    def fg(self):
        return self.get_color(self.colors['fg'])
    
    def bg(self):
        return self.get_color(self.colors['bg'])
    
    def dark_fg(self):
        return self.get_color(self.colors['dark_fg'])

    def dark_bg(self):
        return self.get_color(self.colors['dark_bg'])

    def get_color(self, triplet):
        r,g,b = triplet
        return T.Color(r,g,b)

def colordance(color,min_glow,max_glow,step): #add deviation to formula
    color = util.random_int(0,step) + color
    if color > max_glow:
        color = util.random_int(0,step) + min_glow
    return color

#tile classes - must make a factory generator for them

class WallTile(Tile):
    name = 'Wall'
    char = '#'
    colors = {'fg':     (130,110,150), \
              'bg':     (172,170,173), \
              'dark_fg':(20,20,68), 
              'dark_bg':(7,7,30), \
              'glow':None} 
    description = 'A dull rock wall'
    block_pass = True
    block_gas = True
    block_sight = True
    explored = False
    must_draw = False
    onstep_function = None
    is_submerged = False


class FloorTile(Tile):
    name = 'Floor'
    char = '.'
    colors = {'fg':     (220,220,250), \
              'bg':     (19,19,70), \
              'dark_fg':(30,20,50), 
              'dark_bg':(7,7,30), \
              'glow':None} 
    description = 'Dusty rock floor'
    block_pass = False
    block_gas = False
    block_sight = False
    explored = False
    must_draw = False
    onstep_function = None
    is_submerged = False

class UpStairTile(Tile):
    name = 'Stairs'
    char = '<'
    colors = {'fg':     (250,250,250), \
              'bg':     (19,19,70), \
              'dark_fg':(50,50,50),
              'dark_bg':(7,7,30), \
              'glow':None}
    description = 'Stairs leading up'
    block_pass = False
    block_gas = False
    block_sight = False
    explored = False
    must_draw = True
    onstep_function = None
    is_submerged = False

class DownStairTile(Tile):
    name = 'Stairs'
    char = '>'
    colors = {'fg':     (250,250,250), \
              'bg':     (19,19,70), \
              'dark_fg':(50,50,50),
              'dark_bg':(7,7,30), \
              'glow':None}
    description = 'Stairs leading down'
    block_pass = False
    block_gas = False
    block_sight = False
    explored = False
    must_draw = True
    onstep_function = None
    is_submerged = False


class WaterTile(Tile):

    def __init__(self):
        self.r = 35
        self.g = 22
        self.b = 204

    def bg(self):
        self.g = colordance(self.g, 2, 42, 4)
        self.b = colordance(self.b, 180, 229, 12)
        return self.get_color((self.r, self.g, self.b))
    
    def fg(self):
        r,g,b = (50,50,210)
        #r,g,b = self.colors['fg']
        #b = util.random_int(0,60) - 30 + b
        return self.get_color((r,g,b))

    name = 'Water'
    char = '.'
    colors = {'fg':     None, \
              'bg':     None, \
              'dark_fg':(30,20,50), 
              'dark_bg':(7,7,80), \
              'glow':None} 
    description = 'Murky water'
    block_pass = False
    block_gas = False
    block_sight = False
    explored = False
    must_draw = True
    onstep_function = None
    is_submerged = False
    colordance = True

class DeepWaterTile(WaterTile):
    def __init__(self):
        self.r = 5
        self.g = 2
        self.b = 154

    def bg(self):
        self.g = colordance(self.g, 0, 15, 2)
        self.b = colordance(self.b, 120, 180, 5)
        return self.get_color((self.r, self.g, self.b))

    name = 'Deep Water'
    char = ' '
    is_submerged = True
    block_pass = False
    block_gas = False
    is_safe = False

class GreenWaterTile(WaterTile):
    
    char ='.'
    colors = {'fg':     None, \
              'bg':     None, \
              'dark_fg':(30,20,50), 
              'dark_bg':(7,50,7), \
              'glow':None} 
    
    def __init__(self):
        self.r = 30
        self.g = 100
        self.b = 35

    def bg(self):
        self.r = colordance(self.r, 60, 70, 2)
        self.g = colordance(self.g, 95, 115, 3)
        self.b = colordance(self.b, 50, 70, 4)
        return self.get_color((self.r, self.g, self.b))
    
    def fg(self):
        r,g,b = (60,90,30)
        #r,g,b = self.colors['fg']
        #b = util.random_int(0,60) - 30 + b
        return self.get_color((r,g,b))

class GreenDeepWaterTile(GreenWaterTile):
    char =' '
    def __init__(self):
        self.r = 60
        self.g = 100
        self.b = 25

# #   def bg(self):
# #       self.r = colordance(self.r, 60, 70, 2)
# #       self.g = colordance(self.g, 95, 115, 3)
# #       self.b = colordance(self.b, 20, 40, 4)
# #       return self.get_color((self.r, self.g, self.b))


class MagmaTile(Tile):
    def __init__(self):
        self.r = 180 
        self.g = 180
        self.b = 0

    def bg(self):
#        self.r = colordance(self.r,180,250,15)
#        self.g = colordance(self.g, 0, 60, 5)
        self.r = colordance(self.r,240,255,2)
        self.g = colordance(self.g, 90, 170, 8)
        self.b = colordance(self.b,0,15,2)
#        if (self.g - self.r) > 30:
#            self.g = self.r - 15
        #g = util.random_int(0,(r-10)) 
        return self.get_color((self.r,self.g,self.b))

    def fg(self):
        r,g,b = (0,0,0)
        return self.get_color((r,g,b))

    colors = {
        'fg': None,\
        'bg':None,\
        'dark_fg':(52,22,0), 
        'dark_bg':(30,8,0), \
    }
    name = 'Lava'
    char = ' '
    description = 'Smoldering lava!'
    block_pass=False
    block_gas = False
    block_sight=False
    explored = False
    must_draw = True
    colordance = True
    is_safe = False

class LavaTile(MagmaTile):
    def bg(self):
#        self.r = colordance(self.r, 180,230, 12)
#        self.g = colordance(self.g, 30, 60, 4  );
        self.r = colordance(self.r, 230,245, 3)
        self.g = colordance(self.g, 65, 150, 12  );
        return self.get_color((self.r,self.g,0))
    def fg(self):
        return self.get_color((220,112,4))
    char ="."
    description = 'Pool of lava'


