from logger import log
import libtcodpy as T
import os

SCREEN_HEIGHT = 47
SCREEN_WIDTH = 100
#SCREEN_FONT = 'ibmnew8x12.png'
#SCREEN_FONT = 'Ruterminal_8x8_aa_ro.png'
SCREEN_FONT_DIR = 'fonts/'

# #   for dirname, dirnames, filenames in os.walk(SCREEN_FONT_DIR+'*.png'):
# #       for filename in filenames:
# #           print filename

FONT_LIST = []
FONT_INDEX = 0

for filename in os.listdir(SCREEN_FONT_DIR):
    if ".png" in filename:
        FONT_LIST.append(filename)

log.debug(FONT_LIST)

VERSION = '0.0.7a'

WINDOW_TITLE = 'Alchemyst ' + VERSION
#FPS limit
FPS_LIMIT = 30
T.sys_set_fps(FPS_LIMIT)

#some graphics setup
TORCH_RADIUS = 8
FOV_ALGO = 0 #default algorhym for line of sight
FOV_LIGHT_WALLS = True

#Map settings, our map is bigger than viewport
MAP_WIDTH = 120 #set minimum to viewport size
# MAP_WIDTH = 300 #set minimum to viewport size
MAP_HEIGHT = 80 #set minimum to viewport size
# MAP_HEIGHT = 200 #set minimum to viewport size

# MAP_WIDTH = 80 #set minimum to viewport size
# MAP_HEIGHT = 40 #set minimum to viewport size


#camera settings
CAMERA_WIDTH = 80
CAMERA_HEIGHT = 40


#map data - randomize for each level
ROOM_MAX_SIZE = 11
ROOM_MIN_SIZE = 3
MAX_ROOMS = 100

#UI init
BAR_WIDTH = 20
BAR_HEIGHT = SCREEN_HEIGHT

LOG_WIDTH = CAMERA_WIDTH
LOG_HEIGHT = 7

#console init
CON_MAP = None
CON_BAR = None
CON_LOG = None
CON_MONSTER = None
CON_GAS = None #gas console, maybe unwise


#items init

#Mob init
MAX_ROOM_MONSTERS = 1

#gui
DEFAULT_WINDOW_WIDTH = 60

SAVE_PATH="./save/"

