import DijkstraMap

class AiMap(object):

    def __init__(self, size=5):
        self.dmap = DijkstraMap.DijkstraMap();
        self.size = size

    def reset(self):
        self.dmap = None

    def restore(self):
        self.dmap = DijkstraMap.DijkstraMap();

    def recalc(self, x, y):
        self.dmap.initmap()
        self.dmap.dm_rec([(x, y, 0)], self.size)

    # Works as see/smell/hear target
    def can_sense(self, target):
        if self.dmap.check_lt_max(target.x, target.y): # magic number, DijkstraMap max_dist_valuehh
            return True
        else:
            return False

    def get_dmap(self):
        return self.dmap

    def advance(self, x,y):
        # log.debug(str(["{}".format(i) for i in self.dmap.dmap]))
        return self.dmap.roll_down(x,y)

    def retreat(self, x,y):
        return self.dmap.roll_up(x,y)