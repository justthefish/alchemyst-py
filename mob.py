from globals import *
import ui
import math
import util
import ai
from AiMap import AiMap

def init(game):
    global GAME
    GAME = game

def render():
    for object in GAME.level.objects:
        if object != 'Player':
            object.draw()
    GAME.player.draw()

def is_blocked(x, y):
    if GAME.level.map[x][y].block_pass:
        return True
    for object in GAME.level.objects:
        if object.block_pass and object.x == x and object.y == y:
            return True
    return False


class GameObject(object):
    x = None
    y = None
    char = ' '
    name = ''
    color = None
    bg = T.BKGND_NONE
    block_pass = False

    def __init__(self):
        GAME.level.objects.append(self)

    def draw(self):
        if T.map_is_in_fov(GAME.level.fov_map, self.x, self.y):
            (x,y) = self.to_camera_coordinates(self.x, self.y, GAME)

            if x is not None:
                # T.console_set_foreground_color(GAME.CON_MAP, self.color)
                T.console_set_default_foreground(GAME.CON_MAP, self.color)
                T.console_put_char(GAME.CON_MAP, x, y, self.char, self.bg)
            
    def clear(self):
        #erase me!
        (x,y) = self.to_camera_coordinates(self.x,self.y, GAME)
        if x is not None:
            T.console_put_char(GAME.CON_MAP, x, y, ' ', T.BKGND_NONE)

    def send_to_back(self):
        #send me to the bottom of the buffer, so i can be being overwritten
        global GAME
        GAME.level.objects.remove(self)
        GAME.level.objects.insert(0,self)

    def to_camera_coordinates(self, x, y, game):
        #convert coordinates on the map to coordinates on the screen
            (x, y) = (x - game.camera_x, y - game.camera_y)
            if (x < 0 or y < 0 or x >= CAMERA_WIDTH or y >= CAMERA_HEIGHT):
                    return (None, None)  #if it's outside the view, return nothing
            return (x, y)


class Mob(GameObject):
    max_hp = 10
    hp = max_hp
    defence = 0
    power = 3
    blocks_pass = True
    speed = 3
    backpack = None
    waypoints = []  # list of tiles to next target

    base_ai = ai.BasicMonsterAi

    ai = None       # type of AI used
    item = None

    #ai stuff
    ai_map = None   # dijkstra map around this mob
    ai_map_size = 6 # size
    threat_modifier = 2 # how good monster remembers agressors
    wimpy = 0.25    # level of health on which mob flees
    faction = None  # faction for diplomacy

    def __init__(self):
        self.owner = self
        self.hp = self.max_hp
        self.ai = self.base_ai() #instantiate
        self.ai_map = AiMap(self.ai_map_size) #long map for player

        if self.ai:
            self.ai.owner = self.owner
        self.death_function = self.generic_death

    def pre_save(self):
        self.ai.clean_threats()
        self.ai.state_to('wandering')
        self.ai_map.reset()

    def post_load(self):
        self.ai_map.restore()

    def set_ai(self, ai):
        self.ai = ai

    def can_see(self, target):
        if not isinstance(target, GameObject):
            return False
        # @todo remove this, crutch from ye olde times
        if target is GAME.player:
            if T.map_is_in_fov(GAME.level.fov_map, self.x, self.y):
                return True
            else:
                return False
        else:
            if self.ai_map.can_sense(target):
                return True
            else:
                return False

    def is_adjacent(self, x, y):
        if abs(self.x - x) < 2 and abs(self.y - y) < 2:
            return True
        else:
            return False

    def distance_to(self, other):
        #return the distance
        dx = other.x - self.x
        dy = other.y - self.y
        return math.sqrt(dx ** 2 + dy ** 2)

    def take_damage(self, damage):
        if damage > 0:
            self.hp -= damage
        # todo single place of killing mob, move this to take_turn
        if self.hp <= 0:
            function = self.death_function
            if function is not None:
                function()
    
    def attack(self, target):

        damage = self.power - target.defence

        target.ai.threats.add(id(self), self.power)

        if damage > 0:
            ui.message(self.owner.name.capitalize() + ' hits ' \
                    + target.name + ' for ' \
                    + str(damage) + ' hit points.')
            target.take_damage(damage)
        else:
            ui.message(target.name.capitalize() + ' deflects attack of '\
                    + self.owner.name.capitalize() + ' with ease.')

    def walk(self, dx, dy):
        #move to fighter asap - potions cant walk!
        self.owner = self

        x = self.x + dx
        y = self.y + dy
        return self.move_to_coords(x, y)

    def move_to_coords(self, x, y):

       #try to find target
        target = None
        for obj in GAME.level.objects:
            if isinstance(obj, Mob) and obj.x == x and obj.y == y:
                target = obj
                break

        #attack if wes gotta target, walk otherwise
        if target is not None:
            self.attack(target)
            self.waypoints = [] #we cant continue our pathk
            return True
        else:
            if not is_blocked(x, y):
                # log.debug(str("\tMoving "+str(self)+" to ["+str(x)+","+str(y)+"]"))
                self.x = x
                self.y = y
                GAME.fov_recompute = True
                return True
            else:
                return False


    def generic_death(self):
        ui.message(self.name.capitalize() + ' is dead!', T.red)
        corpse = GameObject()
        corpse.char = '%'
        corpse.block_pass = False
        corpse.Mob = None
        corpse.name = self.owner.name.capitalize() + ' corpse'
        corpse.send_to_back()
        corpse.ai = None
        corpse.x = self.x
        corpse.y = self.y
        corpse.color = self.color
        #cleanup
        for obj in GAME.level.objects:
            if isinstance(obj, Mob):
                obj.ai.remove_threat(id(self))
        GAME.level.objects.remove(self)


    #regen hp and mana, every turn
    def regen(self):
        if self.hp < self.max_hp and util.random_int(0,3) > 2:
            self.hp += util.random_int(1, (self.max_hp // 5))
            if self.hp > self.max_hp:
                self.hp = self.max_hp
        pass
