import libtcodpy as T
import ai
from mob import Mob

class Player(Mob):
    char = '@'
    name = 'Player'
    color = T.white
    max_hp = 100
    ai_map_size = 16

class Orc(Mob):
    char = 'o'
    name = 'Orc'
    color = T.green
    hp = 30
    max_hp = 30
    power = 3

class Troll(Mob):
    char = 'T'
    name = 'Troll'
    color = T.yellow
    hp = 50
    max_hp = 50
    power = 7
    defence = 2
    speed=2

class Bat(Mob):
    char="b"
    name="Bat"
    color=T.Color(210, 90, 245)
    hp=10
    max_hp = 10
    speed=6
    base_ai = ai.WimpyMonsterAi

class Goblin(Mob):
    char="g"
    name="Goblin"
    color=T.light_yellow
    hp=20
    max_hp = 20
    speed=4
    base_ai = ai.WimpyMonsterAi
    wimpy=0.4