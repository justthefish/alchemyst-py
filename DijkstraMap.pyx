from globals import MAP_WIDTH, MAP_HEIGHT

import numpy as np
cimport numpy as np

import mob
import util

from array import array
from cpython cimport array


cdef class DijkstraMap(object):

    cdef public np.ndarray dmap # make public to debug, very slow
    cdef int max_dist_value
    cdef array.array xs
    cdef array.array ys
    cdef array.array costs
    cdef bint even_index

    def __cinit__(self):
        self.dmap = np.zeros([MAP_WIDTH, MAP_HEIGHT], dtype=np.int)
        self.max_dist_value = 32000
        self.xs =       array.array('i', [ -1,   0,   0,   1,   1,  -1,  -1,   1])
        self.ys =       array.array('i', [  0,  -1,   1,   0,   1,  -1,   1,  -1])
        self.costs =    array.array('i', [100, 100, 100, 100, 140, 140, 140, 140])
        self.even_index = 0

        pass

        # that order to avoid diagonal bumping with autopilot

    def initmap(self):
        self.dmap = np.array([[self.max_dist_value for y in xrange(MAP_HEIGHT)]
            for x in xrange(MAP_WIDTH)], dtype=np.int)

    def check_lt_max(self, int x, int y):
        return (not self.out_of_bounds(x,y) and self.dmap[x][y] < self.max_dist_value)

    cdef out_of_bounds(self, int x, int y):
        if (x >= 0 and x < MAP_WIDTH) \
                and (y >= 0 and y < MAP_HEIGHT):
            return False
        else:
            return True


    cdef get_neighbor_tiles(self, int ox, int oy):
    # cdef get_neighbor_tiles(self, ox,oy, check_function=None):
        cdef list result
        cdef int nx,ny,cost
        result = []
        # if check_function is None:
        #     check_function = self.is_tile_walkable
        for nx,ny,cost in zip(self.xs,self.ys,self.costs):
            x = nx + ox
            y = ny + oy
            if not self.out_of_bounds(x,y) and not mob.is_blocked(x,y):
            # if not self.out_of_bounds(x,y) and check_function(x,y):
                result.append((x,y,cost))
        return result

    # @staticmethod
    # cdef is_tile_walkable(self, int x,int y):
    #     for object in game.Game.level.objects:
    #         if object.block_pass and object.x == x and object.y == y:
    #             return True
    #     return not game.Game.level.map[x][y].block_pass

    cdef seed_roots(self, list root_coords):
        """
        Sets some of tile values
        Receives array of [(x,y,value)..] tuples
        """
        for x,y,value in root_coords:
            if not self.out_of_bounds(x,y):
                self.dmap[x][y] = value

    def dm_rec(self, list root_coords, int max_distance=16):
        """
        Prepare map for a single root node
        uses recursive walk, limited distance
        Give coords in a tuple in format (x, y)
        """
        self.seed_roots(root_coords)
        for x,y,value in root_coords:
            self.recursive_walk( x, y, max_distance)

    cdef recursive_walk(self, int x, int y, int max_steps=16, int count=1):
        """
        Get neighbor x, y and compare values in numpy array
        if it's wrong, then set it, incerase counter and call this
        function itself recursively until max_steps are reached
        """
        cdef int nx, ny, distance, neighbor_tile_value, current_tile_value
        if count > max_steps and max_steps != 0:
            return

        current_tile_value = self.dmap[x,y]
        for nx,ny,distance in self.get_neighbor_tiles(x,y):
            neighbor_tile_value = self.dmap[nx][ny]
            if neighbor_tile_value > current_tile_value + distance:
               self.dmap[nx][ny] = current_tile_value + distance
               #recursive call
               self.recursive_walk( nx, ny, max_steps , count + 1)

            # if neighbor_tile_value > current_tile_value + 1:
            #     self.dmap[nx][ny] = current_tile_value + 1
            #     self.recursive_walk( nx, ny, max_steps , count + 1)

    def dm_full(self, root_coords, max_distance=0):
        """
        For multiple root nodes
        """

        # Naive algo

        self.seed_roots(root_coords)
        while ( self.full_run() ) :
            #what a terrible ternary syntax
            self.even_index = (1 if ( self.even_index == 0) else  0)

        # Queue algo

        # self.queued_run(root_coords)


    cdef full_run(self):
        """
        Naive implementation, O = N^2+N
        Full run through array, returns True if any of tile values
        have been changed
        """
        cdef bint changed
        cdef list ranges
        cdef int current_tile_value, neighbor_tile_value, nx, ny, distance

        changed = False
        ranges = [
            [xrange(0,MAP_WIDTH),
            xrange(0,MAP_HEIGHT)],

            [xrange(MAP_WIDTH - 1,0, -1),
            xrange(MAP_HEIGHT - 1,0, -1)]
        ]
        for y in ranges[self.even_index][1]:
            for x in ranges[self.even_index][0]:
                current_tile_value = self.dmap[x][y]
                for nx, ny, distance in self.get_neighbor_tiles(x,y):
                    neighbor_tile_value = self.dmap[nx][ny]
                    if neighbor_tile_value > current_tile_value + distance:
                       self.dmap[nx][ny] = current_tile_value + distance
                    # if neighbor_tile_value > current_tile_value + 1:
                    #     self.dmap[nx][ny] = current_tile_value + 1
                       changed = True

        return changed


    def queue_run(self, int x, int y):
        """
        Here will be queued run, instead of naive implementation
        :param x:
        :param y:
        :return:
        """
        """
        :param x:
        :param y:
        :return:
        """
        # https://github.com/cython/cython-docs/blob/master/src/tutorial/queue_example/queue.pyx
        pass


    cdef cheapest_nb(self, int x, int y, str mode='down'):
        cdef tuple result
        cdef list nb_tiles
        cdef int nx, ny, cost, current_tile_value, neighbor_tile_value
        cdef np.ndarray map_reference

        result = None
        #negative dijkstraMap if we are running FROM
        if mode == 'down':
            map_reference = self.dmap
        else:
            map_reference = np.invert(self.dmap)

        # log.debug(str(mode) +"\n"+ str(map_reference))
        currently_selected = self.max_dist_value
        nb_tiles = self.get_neighbor_tiles(x,y)
        self.shuffle1D(nb_tiles)
        for nx, ny, cost in nb_tiles:
            current_tile_value = map_reference[x][y]
            neighbor_tile_value = map_reference[nx][ny]
            # log.debug("current["+str(x)+","+str(y)+"] is "+str(current_tile_value)+\
            #       " vs nb[",str(nx)+","+str(ny)+"] "+str(neighbor_tile_value)
            if neighbor_tile_value < current_tile_value and neighbor_tile_value < currently_selected:
                currently_selected = neighbor_tile_value
                result = (nx, ny, cost)
        return result

    def roll_down(self, x, y, waypoints=None):
        # log.debug("rolling down from "+ str(x) + str(y) + " found " + str(waypoints))
        if waypoints is None: waypoints = []
        result = self.cheapest_nb(x,y)
        if result is not None:
            nx,ny,cost = result
            waypoints.append((nx,ny,cost))
            if cost > 0: #check cost
                waypoints + self.roll_down(nx, ny, waypoints)
        return waypoints

    def roll_up(self, x,y, waypoints=None):
        if waypoints is None: waypoints = []
        result = self.cheapest_nb(x,y, 'up')
        # log.debug("result of rollup " + str(result))
        if result is not None:
            nx,ny,cost = result
            waypoints.append((nx,ny,cost))
            if cost > (-1 * self.max_dist_value): #check cost
                waypoints + self.roll_up(nx, ny, waypoints)
        else:
            # random tile, run elsewhere!
            # log.debug("Run anywhere, no time to think!")
            nbs = self.get_neighbor_tiles(x,y)
            random_nb_tile = nbs[ util.random_int(0, (len(nbs) -1) ) ]
            waypoints.append(random_nb_tile)
        return waypoints

    cdef shuffle1D(self, list x):
        cdef np.ndarray[long, ndim=1] idx = np.where(~np.isnan(x))[0]
        cdef unsigned int i,j,n,m

        randint = np.random.randint
        for i in xrange(len(idx)-1, 0, -1):
            j = randint(i+1)
            n,m = idx[i], idx[j]
            x[n], x[m] = x[m], x[n]