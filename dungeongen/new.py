import level
import util
from globals import ROOM_MIN_SIZE, ROOM_MAX_SIZE
import tiles
import bestiary.monsters as Mon

# From this class we must get two things:
# 1. List of room center coords, used by patrol routine of ai
# 2. List of "spawn points", used by populate algo, then discarded
# 3. Changed game map, filled with instances of Tile class

class Gen(object):

    depth = 0
    smap = []
    rooms = []
    stairs = []
    centers = [] #list of room centers
    spawns = [] #list o spawn points
    monsters = []

    def __init__(self, width, height, depth):
        self.count = 0

        # fill symbolic map with walls
        self.smap = [[' ' for iy in xrange(height+1)] for ix in xrange(width+1)]
        # @todo load some from external files, each method must return Room instance
        self.room_chances = {
            self.genRect: 1
        }
        self.width = width
        self.height = height
        self.depth = depth
        self.monsters = []
        self.centers = []
        self.rooms = []
        self.stairs = []
        self.spawns = []


    def add_rooms(self, max_rooms, stairs):

        for r in range(max_rooms):
            choice = util.random_choice(self.room_chances)
            new_room = choice() #pre-rendered symbloic map,
            #create room

            #check for intersects
            failed = False
            for other_room in self.rooms:
                if new_room.intersects(other_room):
                    failed = True
                    break
            if not failed:
                self.rooms.append(new_room)
                self.centers.append( new_room.anchor )
                self.spawns.append( new_room.spawns )
        #blit rooms
        for room in self.rooms:
            for y in xrange(room.y1, room.y2+1):
                for x in xrange(room.x1, room.x2+1):
                    self.smap[x][y] = room.get_glyph(x,y)
            # self.print_map()
        # connect!
        self.connect_rooms()
        return self.centers, self.spawns, self.stairs

    def connect_rooms(self):
        for index, room in enumerate(self.rooms):
            if index == 0:
                pass
            else:
                #toss a cion
                self.connect(room, self.rooms[index-1])

    def connect(self, room1, room2):
        prev_x, prev_y = room2.anchor
        new_x, new_y = room1.anchor
        if util.random_int(0,1) == 1:
            #horizontal then vertical tunnel
            self.create_h_tunnel(prev_x, new_x, prev_y)
            self.create_v_tunnel(prev_y, new_y, new_x)
        else:
            #inverted order
            self.create_v_tunnel(prev_y, new_y, prev_x)
            self.create_h_tunnel(prev_x, new_x, new_y)

    def create_h_tunnel(self, x1,x2,y):
        # @todo add doors
        for x in range(min(x1,x2), max(x1,x2) + 1):
            self.smap[x][y] = '.'

    def create_v_tunnel(self, y1,y2,x):
        # @todo add doors
        for y in range(min(y1,y2), max(y1,y2) + 1):
            self.smap[x][y] = '.'

    def genRect(self):
            #width and height by random. seed 0 - @todo:make timestamp a seed
            w = util.random_int(ROOM_MIN_SIZE, ROOM_MAX_SIZE)
            h = util.random_int(ROOM_MIN_SIZE, ROOM_MAX_SIZE)
            #random position inside map
            x = util.random_int(0, self.width - w - 2)
            y = util.random_int(0, self.height - h - 2)
            #room center
            cx = (x + int(w / 2))
            cy = (y + int(h / 2))

            def walls(ix, iy, w,h, cx, cy):

                glyph = '.'
                if ix == 0:
                    glyph = "x"
                elif ix == (w) :
                    glyph = "x"
                elif iy == 0:
                    glyph = "x"
                elif iy == (h):
                    glyph = "x"
                return glyph

            smap = [[walls(ix,iy, w, h, cx, cy) for iy in xrange(0,h+1)] for ix in xrange(0, w+1)]
            return Room(x,y,w,h,smap,(cx,cy),[(cx,cy)])

    def print_map(self):
        for y in xrange(self.height):
            str = "";
            for x in xrange(self.width):
                str += self.smap[x][y]
            print str+"\n"

    def add_stairs(self, stairs):
        stairs_dict = {
            1: {
                "<": 1,
                ">": 3
            },
            15: {
                "<": 3,
                ">": 0
            },
            "other": {
                "<": 3,
                ">": 3,
            }
        }
        # print "stairs keys",stairs_dict.keys()
        if self.depth in stairs_dict.keys():
            key = self.depth
        else:
            key = "other"
        # print "stairs key", key

        for direction in stairs_dict[key].keys():
            for i in range(stairs_dict[key][direction]):
                room = self.rooms[util.random_int(0,(len(self.rooms) -1) )]
                x,y = room.get_random_coords()
                self.smap[x][y] = direction
                stairs.append({
                    "x":x,
                    "y":y,
                    "dir": direction,
                    "target": None
                })


    def draw_rooms(self, map, branch, depth):
        for y in range(0, self.height+1):
            for x in range(0, self.width+1):
                map[x][y] = self.get_tile(self.smap[x][y])
        return map

    def get_tile(self, glyph):
        # @todo load from extarnal file
        tile_map = {
            ".": "Floor",
            " ": "Wall",
            "x": "Wall",
            "w": "Water",
            "W": "DeepWater",
            "<": "UpStair",
            ">": "DownStair",
        }
        t = tile_map[glyph] + "Tile"
        # call of FloorTile for instance
        return getattr(tiles, t)()

    def itemize(self, branch, depth):
        return []

    def populate(self, branch, depth):
        max_room_monsters = 5
        max_monsters = 100
        monster_map = {
            None: 100,
            "Orc" : 20,
            "Goblin": 15,
            "Troll": 2,
            "Bat": 10
        }
        mcount = 0
        for room in self.rooms:
            num_monsters = util.random_int(0,max_room_monsters)
            for i in range(num_monsters):
                #get spawn
                mon = util.random_choice(monster_map)
                if mon is not None:
                    mcount += 1
                    monster = getattr(Mon, mon)()
                    monster.x, monster.y = room.get_random_coords()
                    self.monsters.append(monster)
            if mcount > max_monsters:
                break
        return self.monsters

class Room(object):
    #rectangle room for map
    def __init__(self, x,y,w,h, smap, anchor, spawns):
        self.x1 = x
        self.y1 = y
        self.x2 = x + w
        self.y2 = y + h
        self.smap = smap
        self.anchor = anchor
        self.spawns = spawns

    def intersects(self, other):
        """
        :param other:Room
        :return:
        """
        #true if rectangle intersects with other one
        if (self.x1 <= other.x2 and self.x2 >= other.x1 and
                self.y1 <= other.y2 and self.y2 >= other.y1):
            result = False
            #check room tile intersection
            for y in range (self.y1, self.y2+1):
                for x in range(self.x1, self.x2+1):
                    if other.in_bounds(x,y):
                        #if "symbolic tile" intersects are incompatible
                        if not self.add_glyphs(self.get_glyph(x,y), other.get_glyph(x,y)):
                            return True
                    pass
            return result
        else:
            return False

    def in_bounds(self, x, y):
        return (x >= self.x1 and x <= self.x2 \
               and y >= self.y1 and y <= self.y2)

    def get_glyph(self, x,y):
        return self.smap[x - self.x1][y - self.y1] #reduce index

    def get_random_coords(self):
        return (util.random_int(self.x1+1, self.x2-1), util.random_int(self.y1+1, self.y2-1))

    @classmethod
    def add_glyphs(cls, glyph1, glyph2):
        # print "compare:", glyph1, glyph2, "\n\n"
        # if cls.reduce_glyph(glyph1) == cls.reduce_glyph(glyph1):
        #     return glyph1
        # if glyph1 == "@":
        #     return glyph2
        # if glyph2 == "@":
        #     return glyph1
        return False

    @staticmethod
    def reduce_glyph(glyph):
        reduced = "$()\<>8[]" #item designations
        if glyph in reduced:
            return "."
        else:
            return glyph
