import libtcodpy as T
import util
from tiles import *

def get_rooms():
    return rooms

def get_gamemap():
    return game_map

def get_roomcenters():
    return room_centers

def generate():
    global game_map, rooms, room_centers
    import simple
    simple.generate()
    game_map = simple.game_map
    room_centers = simple.room_centers
    rooms = simple.rooms
    
    rivermap = T.heightmap_new(MAP_WIDTH, MAP_HEIGHT)
    tileset = [WaterTile, DeepWaterTile]
    # tileset = [GreenWaterTile, GreenDeepWaterTile]
    # tileset = [LavaTile, MagmaTile]
    # for i in range(util.random_int(1,5)):
    #    addlake(rivermap)
    addriver(rivermap)
    finalize(rivermap, tileset)
    T.heightmap_delete(rivermap)
    
def addriver(rivermap):
    x_start=0;
    y_start = util.random_int(0,MAP_HEIGHT-1)
    
    dig = 1
    depths = [util.random_int(3,6), None]
    widths = [util.random_int(2,5), None]
    while dig:
        xs = []
        ys = []
        x_iter = x_start + util.random_int(30,40)

        if MAP_WIDTH - x_iter < 20:
            x_iter = MAP_WIDTH;

        y_iter = y_start + util.random_int(0,60) - 30
        if x_iter > MAP_WIDTH -1:
            x_iter = MAP_WIDTH -1
            dig = 0
        if y_iter < 0:
            y_iter = 0
        if y_iter > MAP_HEIGHT - 1:
            y_iter = MAP_HEIGHT -1
        #draw river
        xs.append(x_start)
        xs.append(util.random_int(x_start, x_iter))
        xs.append(util.random_int(x_start, x_iter))

        xs.append(x_iter)
        
        ys.append(y_start)
        #ys.append( util.random_int(0, 60) - 30 )
        #ys.append( util.random_int(0, 60) - 30 )
        ys.append(y_start + util.random_int(0,20) - 10)
        ys.append(y_iter + util.random_int(0,20) - 10)
        ys.append(y_iter)

        depths[1] = util.random_int(3,6);
        widths[1] = util.random_int(2,5);

        dig_controlled_bezier(rivermap, xs, ys, depths, widths)
        depths[0] = depths[1]
        widths[0] = widths[1]
        x_start = x_iter 
        y_start = y_iter


def addlake(rivermap):

    xs = []
    ys = []
    for i in xrange(6):
        xs.append( util.random_int(0, MAP_WIDTH-1))
        ys.append( util.random_int(0, MAP_HEIGHT-1))
    dig_bezier(rivermap, xs,ys)


def dig_bezier(rivermap, xs, ys):
    T.heightmap_dig_bezier(rivermap, xs, ys, 3, 5, 5, 4)
    #T.heightmap_dig_bezier(rivermap, xs, ys, 10, 4, -10, 3)

def dig_controlled_bezier(rivermap,xs,ys, depths, widths):
    T.heightmap_dig_bezier(rivermap,xs,ys, widths[0], depths[0], widths[1], depths[1])


def finalize(rivermap, tileset):
    for x in range(MAP_WIDTH):
        for y in range(MAP_HEIGHT):
            
            value = T.heightmap_get_value(rivermap, x, y)
            if value >0 and value < 3: 
                game_map[x][y] = tileset[0]()
            elif value >=3:
                game_map[x][y] = tileset[1]()


    for x in range(MAP_WIDTH):
        game_map[x][0] = WallTile()
        game_map[x][MAP_HEIGHT-1] = WallTile()

    for y in range(MAP_HEIGHT):
        game_map[0][y] = WallTile()
        game_map[MAP_WIDTH-1][y] = WallTile()



