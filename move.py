from globals import MAP_WIDTH, MAP_HEIGHT
import DijkstraMap, ui
import libtcodpy as T
import random
import mob

#test

def stop_autopilot(game):
    game.state = 'playing'
    game.player.waypoints = []
    ui.message('Autopilot disengaged', T.gray)

def autopilot(game):
    if not ui.readkey():
        game.state = 'autopilot'
    else:
        stop_autopilot(game)

    if not game.player.waypoints:
        """
        seed unexplored as rootnodes
        """
        unexplored = []

        for y in range(MAP_HEIGHT):
           for x in range(MAP_WIDTH):
               if game.level.map[x][y].explored == False \
                   and game.level.map[x][y].block_pass == False:
                    unexplored.append((x,y,0))
        
        dm = DijkstraMap.DijkstraMap()
        dm.initmap()
        dm.dm_rec(unexplored)
        wps = dm.roll_down(game.player.x, game.player.y)
        if not wps:
            dm.dm_full(unexplored)
            wps = dm.roll_down(game.player.x, game.player.y)
        game.player.waypoints = wps
        del dm
    else:
        #time.sleep(0.04) #give GC a breath
        pass

    if walk_to_next_wp(game.player):
        pass
    else:
        game.player.waypoints = []
        game.state = 'playing'
        ui.message('Finishing exploring', T.dark_orange)

def walk_to_next_wp(actor):
    try:
        x, y, _ = actor.waypoints.pop(0)
        return actor.move_to_coords(x,y)
    except:
        return False

def get_random_neighbor_tile(ox,oy):
    xs =    [ -1,   0,   0,   1,   1,  -1,  -1,   1]
    ys =    [  0,  -1,   1,   0,   1,  -1,   1,  -1]
    costs = [100, 100, 100, 100, 140, 140, 140, 140] #not used yet
    # nbs = random.shuffle(zip(xs, ys, costs))
    nbs = zip(xs, ys, costs)
    random.shuffle(nbs)
    i = 0
    x,y, cost = nbs[0]
    while (mob.is_blocked(ox+x,oy+y) and i < 7):
        i = i + 1
        x, y, cost = nbs[i]
    return ox+x , oy+y, cost

def set_random_path(actor, path_len=4):
    ox, oy = actor.x, actor.y
    wps =[]
    while path_len > 0:
        ox, oy, cost = get_random_neighbor_tile(ox,oy)
        wps.append((ox, oy, cost))
        path_len = path_len - 1
    return wps
