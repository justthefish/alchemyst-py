Programming:

1. Speed system
    variants: 
        1) slot system, like 5 slots in turn,
        fastest + hasted make turns in all slots
        fastest make turns in 1,2,4,5 slots
        fast creatures make turns in slots 1,3,5
        normal creatures get turns in slots 2,4
        slow cretures get turns in slot 3

        2) Turn system (ADOM-like)

        3) Queue system (DCSS-like)

2. Spill model for lava/water/gas distribution

3. Monster AI  
    variants:
        1) tutorial-like
        2) Final state machines
        3) Behavior tree (combines both)
    strategies:
        - dumb walk and attack on sight
        - ranged kiting (ranged summoner spammals)
        - smart retreat, lure to dangerous terrain/traps
        - group attack (surround, block exits, defend ranged attackers, 
          apply smart crowd control)
    features
        - tracking (inter-level scent system?)
        - sounds and attention
        - monster states (sleep/bored/attacking/seeking help/retreating/hunting)

4. Rendering 
    v Camera (lock @ in center, big levels)
    - Console for gas (transparency)
    - Speech bubbles

5. UI
    - Dialog screens (highlight selection, scroll, race/class menu)
    - Bars for hp/buffs etc
    - Prompt for input
    - Log scrolling (and non-blocking --more--)

6. Targeting state
    - new console for targeting

7. Save/load level state and level advancment
    http://stackoverflow.com/questions/198692/can-i-pickle-a-python-dictionary-into-a-sqlite3-text-field
    use the power Luke!
    http://stackoverflow.com/questions/2392732/sqlite-python-unicode-and-non-utf-data
    ALSO!1

8. Moar dungeon generators

9. Inventory

10. Combat

11. Magic

12. Win condition

13. Liquids (water and stuff) features
    - FOV calculation including submerged status
        - modify monster can_see
        - Modify FOV_recalculate
    - All liquids do not save scent!!

14. Gas features
    - FOV reduction
    - Gas erases cancels scent detection on terrain or erases it completely

??. Quest system (as if we need one)

??. Color effects
    - Zerk
    - Blinded
    - Flash
    - Infrared
    - Ambient lighting



old feature list:

1. Lighting 
    1.1 Randomize colors for tiles
    1.2 Make torch dim and flicker
    1.3 Gas, water, lava, mist, smoke flickering 

2. Rewrite tiles part
    2.1 Tile object                                         #done, perhaps too hard
        2.1.1 Walls
        2.1.2 Floor
        2.1.3 Door                                          #need impleentation
        2.1.4 Soil                                          #need impleentation
        2.1.5 Water and deep water                          #need impleentation
        2.1.6 Lava                                          #need implemenation
        2.1.7 ITS A TPAR!
    2.2 Layers
        2.2.1 Gases
        2.2.2 Mist
        2.2.3 Smoke
        2.2.4 Light/darkness
        2.2.5 Fire
    2.3 Walk-by-tile effects
        2.3.1 Lava BURNS!!!
        2.3.2 Swimming/diving
        2.3.3 Gas/smoke reduces sight
        2.3.4 Gas effects (hallucinate/stagger/enflame/stuck and so on) 
    2.4 Moar level generation options (cave-like, water bodies and etc)

3. Polish UI
    3.1 Item stacking                                           #done for inventory
    3.2 multipage menu
    3.3 Cursor selector
    3.4 Selectable lists for dropping
    3.5 Main menu with exit option =)

4. Make heaps of items (item stacking an tiles)

5. Turn hardcoded items into a giant array

6. Monsters state
    6.1 State
        6.1.1 Stand
        6.1.2 Sleep
        6.1.3 Rest/Sit
        6.1.4 Sneak
        6.1.5 TRACE_THE_SCENT(!) - monster smells for player    #done
    6.2 Monster inventory (via array or random)                 #partly done
    6.3 Items using by monsters                                 #partly done
    6.4 Monsters tactics (behaviour pattern)                    #needs more dijkstra
        6.4.1 Fight till death
        6.4.2 Flee when damaged
        6.4.3 Kite (for ranged and/or fast)
        6.4.5 Group tactics
    6.5 Monsters spells (player's too =)                        #partly done
    6.6. SPEED and side-effects

7. Buffs/debuffs (check every tick, effect once a number of tick, at start, at end)
    7.1 Levitate (and check against walk-by-tile effects)
    7.2 Berserk (and bloody screen effect)
    7.3 Infravision (red effect + blind on light)
    7.4 Limb cripple (and side-effects).

8. Hordes/groups, races and relations
    8.1 Race
        8.1.1 Hunam
        8.1.2 Orc
        8.1.3 Goblin
        8.1.4 Kobold
        8.1.5 Elf
        8.1.2 Troll
        8.1.2 Gnoll
        8.1.2 Demon
        8.1.2 Undead
        

9. Work with files - saving level, loading level, post-load processing

10. Smart pathfinding (Dijkstra/A*)                         #dijksta for pf, no dijkstra maps

11. Quests and triggers (dont know how yet)

12 Global map (if we need one)

13 Combat and Equipment slots (we need them. probably.)

14 Item action/interaction (pour, dip, pump gas to smth, fill with liquid)
    14.1 Booze!

15 Hunger/thirst for monster object (make monsters search for food too! =)

16 Levels/skills and progression (dont know how yet)

17 Score and logs/chardumps. YAY! We need them for sure.
