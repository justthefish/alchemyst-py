import libtcodpy as T
from globals import *
import ui

#this is just for testing git
#not really for running items
class Item(object):
    def __init__(self,use_function):
        self.use_function = None


    def pick_up(self, carrier):
        if (carrier.backpack.is_full()):
            ui.message('You cannot carry anymore items.');
        else:
            carrier.backpack.append(self.owner)
            GAME.objects.remove(self.owner)
            ui.message('You picked up a '+ self.owner.name, T.green);

    def use(self, carrier):
        if self.use.function == None:
            ui.message('The ' + self.owner.name + ' cannot be used.' )
        else:
            if self.use_function(self, carrier) != 'cancelled':
                ui.message('Debug. used ' + self.owner.name)
                carrier.backpack.remove(self.owner)

class Backpack:
    def __init__(self, capacity=5):
        self.inventory = []
        self.capacity = capacity

    def is_full():
        return self.inventory.count >= capacity

    def remove(item):
        self.inventory.remove(item)

    def append(item):
        self.inventory.append(item)
