import libtcodpy as T
import ui
import util
import move
from logger import log

import weakref, ctypes

class ThreatList(object):

    def __init__(self):
        # self.threats = weakref.WeakKeyDictionary()
        self.threats = {}
        pass

    def get(self, key):
        return self.threats[key]

    def set(self, key, value):
        self.threats[key] = value
        return True

    def add(self, key, value):
        try:
            self.threats[key] += value
        except:
            self.set(key,value)

    def get_most_threatening(self):
        if self.threats:
            sorted_threats = sorted(self.threats.items(), reverse=True ,key=lambda x: x[1])
            target_id, threat = sorted_threats[0]
            return target_id
        else:
            return None
    def decay(self, rate=1):
        todelete = []
        for k in self.threats:
            self.threats[k]  -= rate
            if self.threats[k] < 2:
                todelete.append(k)
        for k in todelete:
            del self.threats[k]
        del todelete

    def clean(self):
        todelete = []
        for k in self.threats:
            todelete.append(k)
        for k in todelete:
            del self.threats[k]

    def remove_id(self, id):
        self.threats.pop(id, None)

class GenericAi(object):
    def __init__(self):
        self.owner = None
        self.state = 'wandering'
        self.threats = ThreatList()

    def take_turn(self):
        #just an interface
        pass

    def clean_threats(self):
        self.threats.clean()

    def state_to(self, string):
        self.owner.waypoints = []
        self.state = string

    def remove_threat(self, id):
        self.threats.remove_id(id)

    def stagger(self):
        if self.owner.waypoints:
            move.walk_to_next_wp(self.owner)
        else:
            self.owner.waypoints = move.set_random_path(self.owner)

class TestAi(GenericAi):

    def take_turn(self):
        pass

# @todo rewrite with proper stacked finite state machine

class BasicMonsterAi(GenericAi):

    def __init__(self):
        # super(BasicMonsterAi, self).__init__()
        self.states = {
            "attacking": self.attacking,
            "hunting": self.hunting,
            "wandering": self.wandering,
            "sleeping": self.sleeping,
        }
        self.state = 'wandering'
        self.threats = ThreatList()

    def take_turn(self):
        monster = self.owner
        target = self.threats.get_most_threatening()
        if target:
            target = weakref.proxy(ctypes.cast(target, ctypes.py_object).value)

        if self.state != 'wandering':
            log.debug(monster.name +" "+ str(monster) +"["+str(monster.x)+","+str(monster.y)+"] "+ str(self) + \
                      " " + str(self.state) +" "+str(self.threats.get_most_threatening())+" "+ \
                      str(monster.waypoints) +" "+ str(self.states[self.state]))
        if self.state == 'attacking':
            log.debug("threats:"+repr(self.threats.get_most_threatening()))

        self.states[self.state](target)
        del target #remove strong ref

    def wandering(self, target):
        #forgive enemies slowly
        self.threats.decay()
        #if has threats - attack
        if target is not None:
            self.state_to('attacking')
        else:
            #just move randomly
            self.stagger()
            #consider 3% chance to sleep, consider more if health is low
            # if not self.owner.can_see(target) and util.random_int(0,100) > \
            #     (((self.owner.max_hp - self.owner.hp) / self.owner.max_hp) * 100 - 3):
            #     self.state_to('sleeping')

    def sleeping(self, target):
        self.threats.decay(3)
        self.owner.regen() #double roll for regen
        if target is not None:
            self.state_to('attacking')
        #wakeup
        elif util.random_int(0,100) > 20:
            self.state_to('wandering')

    def attacking(self, target):
        # ui.message(monster.name.capitalize() + " screams and attacks!", T.red);
        #this is a placeholer really, would make diplomacy and threat list
        # self.threats.decay()

        if target:
            # log.debug(str(monster) + "'s target " + str(target))
            if self.owner.distance_to(target) <=1.5 \
                    and target.hp > 0:
                self.owner.attack(target)
            elif not self.owner.can_see(target):
                self.state_to('hunting')
            elif not self.owner.waypoints:
                if target.ai_map is not None:
                    self.owner.waypoints = target.ai_map.advance(self.owner.x, self.owner.y)
                    move.walk_to_next_wp(self.owner) #attack!
            else:
                move.walk_to_next_wp(self.owner) #attack!

        else:
            self.state_to('wandering')

    def hunting(self, target):
        #forgives enemy slowly while chasing
        self.threats.decay()
        if target:
            if self.owner.can_see(target):
                ui.message(self.owner.name.capitalize() + ' sees his nemesis, '+\
                           target.name + ', and attacks!', T.cyan)
                self.state_to('attacking')
            elif not self.owner.waypoints:
                self.owner.waypoints = target.ai_map.advance(self.owner.x, self.owner.y)
                move.walk_to_next_wp(self.owner) #attack!
            else:
                move.walk_to_next_wp(self.owner) #attack!
        else:
            self.state_to('wandering')


class WimpyMonsterAi(BasicMonsterAi):

    def __init__(self):
        super(BasicMonsterAi, self).__init__()

        self.states = {
            "sleeping": self.sleeping,
            "attacking": self.attacking,
            "fleeing": self.fleeing,
            "wandering": self.wandering,
            "hunting": self.hunting
        }

    def attacking(self, target):
        # ui.message(monster.name.capitalize() + " screams and attacks!", T.red);
        #this is a placeholer really, would make diplomacy and threat list
        if target:
            # log.debug(str(monster) + "'s target " + str(target))

            if self.owner.distance_to(target) <=1.5 \
                    and target.hp > 0:
                self.owner.attack(target)
            elif not self.owner.waypoints:
                self.owner.waypoints = target.ai_map.advance(self.owner.x, self.owner.y)
                move.walk_to_next_wp(self.owner) #attack!
            else:
                move.walk_to_next_wp(self.owner) #attack!

            #consider taken damage and turn to flee
            if self.owner.hp < self.owner.max_hp*self.owner.wimpy:
                self.state_to('fleeing')
                if self.owner.can_see(target):
                    ui.message(self.owner.name.capitalize() + \
                        ' is overwhelmed and turns to flee!', T.light_gray)
        else:
            self.state_to('wandering')


    def fleeing(self, target):
        if target:
            if not self.owner.can_see(target):
                self.threats.decay() # lose threat away from target

            if not self.owner.waypoints:
                self.owner.waypoints = target.ai_map.retreat(self.owner.x, self.owner.y)
            else:
                mx, my, _ = self.owner.waypoints[0]
                if target.is_adjacent(mx, my): #in aggressor vicinity!
                    log.debug("next move brings close to aggressor, recalc route!")
                    # @todo fix this with more omplicated heuristics
                    self.owner.waypoints = target.ai_map.retreat(self.owner.x, self.owner.y)

            if self.owner.hp < self.owner.max_hp*0.8:
                move.walk_to_next_wp(self.owner) #flee!

            #consider health and threat and turn to attack/hunt/wander
            # if health is low and still can get path out
            #  of inverted dmap - continue fleeing
            # if health is ok and can see any threat - attack

            else:
                self.state_to('attacking')
                if self.owner.can_see(target):
                    ui.message(self.owner.name.capitalize() + \
                        ' suddenly feels brave and turns around to attack!', T.dark_red)
            # if in good health and have any
            #  high threat (more than half of max hp) - hunt

            # else wander - nothing to worry about
        else:
            self.state_to('wandering')
