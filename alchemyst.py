#!/usr/bin/python
import sys, os
if __name__ == '__main__' and __package__ is None:
    sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), './lib')))

from game import Game
#running game
if __name__ == '__main__':
     wizard = 'wizard' in sys.argv
     Game(wizard).play()