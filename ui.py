import libtcodpy as T
from globals import *
from logo import logo
import textwrap
from logger import log

#keys map
from keymap import *

def decode_key(key):
    for keys, cmd in KEYS:
        if key in keys:
            return cmd
    return None


def init(game):
    global GAME, CON_MAP, CON_BAR, CON_LOG, CON_MONSTER, log
    
    #global game object
    GAME = game

    cycle_font(True)

    #init consoles
    game.CON_MAP = T.console_new(MAP_WIDTH, MAP_HEIGHT)
    game.CON_OVERLAY = T.console_new(MAP_WIDTH, MAP_HEIGHT)
    T.console_set_default_background(game.CON_OVERLAY,T.black)
    T.console_set_key_color(game.CON_OVERLAY, T.black) #ignore black tiles

    game.CON_BAR = T.console_new(BAR_WIDTH, BAR_HEIGHT)
    game.CON_LOG = T.console_new(LOG_WIDTH, LOG_HEIGHT)
    game.CON_MONSTER = T.console_new(MAP_WIDTH, MAP_HEIGHT)
    game.CON_GAS = T.console_new(MAP_WIDTH, MAP_HEIGHT)
    #set black to transparent for gas
    T.console_set_key_color(game.CON_GAS, T.black)
    
    game.fov_recompute = True
    #message log is empty
    game.log = []


def cycle_font(init_run = False):
    global FONT_LIST, FONT_INDEX
    
    FONT_INDEX = (FONT_INDEX + 1) % len(FONT_LIST)

    SCREEN_FONT = FONT_LIST[FONT_INDEX]

    if not init_run: 
        message('Changed font to ' + SCREEN_FONT, T.Color(100,100,100))

    if init_run:
        SCREEN_FONT = 'ibmnew8x12.png'
        font_order = T.FONT_LAYOUT_ASCII_INROW
    elif "_ro" in SCREEN_FONT:
        font_order = T.FONT_LAYOUT_ASCII_INROW
    elif "_tc" in SCREEN_FONT:
        font_order = T.FONT_LAYOUT_TCOD
    elif "_as" in SCREEN_FONT:
        font_order = T.FONT_LAYOUT_ASCII_INCOL
    else:
        font_order = T.FONT_LAYOUT_ASCII_INROW

    font = SCREEN_FONT_DIR + SCREEN_FONT

    T.console_set_custom_font(font, T.FONT_TYPE_GREYSCALE | font_order)
    T.console_init_root(SCREEN_WIDTH, SCREEN_HEIGHT, WINDOW_TITLE, False)

def close():
    T.console_delete(GAME.CON_MAP)
    T.console_delete(GAME.CON_BAR)
    T.console_delete(GAME.CON_LOG)
    T.console_delete(GAME.CON_MONSTER)
    T.console_delete(GAME.CON_GAS)

def log_render():
    T.console_clear(GAME.CON_LOG)
    y = 0
    for (line, color) in GAME.log:
        # T.console_set_foreground_color(GAME.CON_LOG,color)
        T.console_set_default_foreground(GAME.CON_LOG,color)
        T.console_print_ex(GAME.CON_LOG, 0, y, T.BKGND_NONE, T.LEFT, line)
        y +=1
    T.console_blit(GAME.CON_LOG, 0,  0,  80, (SCREEN_HEIGHT - CAMERA_HEIGHT), 0,  20, CAMERA_HEIGHT)

def title_screen():
    title_screen = logo
    
    T.console_clear(None)
    # T.console_print_center(None, SCREEN_WIDTH/2, 5, T.BKGND_NONE, title_screen)
    T.console_print_ex(None, SCREEN_WIDTH/2, 5, T.BKGND_NONE, T.CENTER, title_screen)
    T.console_flush()
    waitkey()
    T.console_clear(None)

#function from previous version
def message(new_msg, color=T.white):
        
    new_msg_lines = textwrap.wrap(new_msg, LOG_WIDTH)
    for line in new_msg_lines:
        #buffer full - go f*ck urself
        if len(GAME.log) == LOG_HEIGHT:
            del GAME.log[0]
        #add new message
        GAME.log.append((line,color))


def readkey():
    #while True:
        #key = T.console_wait_for_keypress(True)
        #key = T.console_wait_for_keypress(True)
        key = T.console_check_for_keypress(True)
        #Logger.log(key.vk, repr(chr(key.c)) )
        if key.vk in [T.KEY_SHIFT, T.KEY_CONTROL, T.KEY_ALT,
                    T.KEY_CAPSLOCK]:
            #continue
            pass            
        if key.c != 0 and chr(key.c) not in '\x1b\n\r\t':
            s = chr(key.c)
            if key.shift:
                s = s.upper()
            if key.lalt or key.ralt:
                s = 'Alt+' + s
            if key.lctrl or key.rctrl:
                s = 'Ctrl+' + s
            return s
        elif key.vk != 0:
            return key.vk
        else:
            return None
    
def waitkey():
    while True:
        key = T.console_wait_for_keypress(True)
        if key.vk in [T.KEY_SHIFT, T.KEY_CONTROL, T.KEY_ALT,
                    T.KEY_CAPSLOCK]:
            continue
        if key.c != 0 and chr(key.c) not in '\x1b\n\r\t':
            s = chr(key.c)
            if key.shift:
                s = s.upper()
            return s
        elif key.vk != 0:
            return key.vk
        else:
            return None


### old functions

def render_bar(x, y, total_width, name, value, maximum, bar_color, back_color):
    #render a bar (HP, experience, etc). first calculate the width of the bar
    bar_width = int(float(value) / maximum * total_width)
 
    #render the background first
    T.console_set_default_background(CON_BAR, back_color)
    T.console_rect(CON_BAR, x, y, total_width, 1, False, T.BKGND_SCREEN)
 
    #now render the bar on top
    T.console_set_default_background(CON_BAR, bar_color)
    if bar_width > 0:
        T.console_rect(CON_BAR, x, y, bar_width, 1, False, T.BKGND_SCREEN)

    #finally, some centered text with the values
    T.console_set_default_foreground(CON_BAR, T.white)
    T.console_print_ex(CON_BAR, x + total_width / 2, y, T.BKGND_NONE, T.CENTER,
        name + ': ' + str(value) + '/' + str(maximum))


def menu(title, header, options, width = DEFAULT_WINDOW_WIDTH, show_selector = True):
    if len(options) > 26: raise ValueError('Cannot have a menu with more than 26 options.')

    #calculate total height for the header (after auto-wrap) and one line per option
    # header_height = T.console_height_left_rect(CON_MAP, 0, 0, width, SCREEN_HEIGHT, header)
    header_height = T.console_get_height_rect(CON_MAP, 0, 0, width, SCREEN_HEIGHT, header)
    height = len(options) + header_height + 4
    #create an off-screen console that represents the menu's window
    window = T.console_new(width, height)

    #print the header, with auto-wrap
    # T.console_set_foreground_color(window, T.white)
    T.console_set_default_foreground(window, T.white)
    T.console_print_frame(window, 0, 0, width, height, False, 0,  title)
    # T.console_print_left_rect(window, 2, 2, width-4, height-2, T.BKGND_NONE, header)
    T.console_print_rect_ex(window, 2, 2, width-4, height-2, T.BKGND_NONE, T.LEFT, header)

    #print all the options
    y = header_height + 2
    letter_index = ord('a')
    for option_text in options:
        text = option_text
        if (show_selector == True):
            text = '(' + chr(letter_index) + ') ' + text
        # T.console_print_left(window, 2, y, T.BKGND_NONE, text)
        T.console_print_ex(window, 2, y, T.BKGND_NONE, T.LEFT, text)
        y += 1
        letter_index += 1

    #blit the contents of "window" to the root console
    x = SCREEN_WIDTH/2 - width/2
    y = SCREEN_HEIGHT/2 - height/2
    T.console_blit(window, 0, 0, width, height, 0, x, y, 1.0, 0.7)

    #present the root console to the player and wait for a key-press
    T.console_flush()
    key = T.console_wait_for_keypress(True)
    #convert the ASCII code to an index; if it corresponds to an option, return it
    index = key.c - ord('a')
    if index >= 0 and index < len(options): return index
    return None

def display_help():
    text = []
    text.append('hjklyubn 12346789 or arrow keys - move')
    text.append('s or . - pass turn')
    text.append('i - inventory')
    text.append('g or , - pick up item')
    text.append('? - this screen')
    text.append('o - turn on autoexplore')
    text.append('F10 - change font size')
    text.append('F9 - toggle AI debug mode')
    return menu('key help', 'Key bindings:\n', text, DEFAULT_WINDOW_WIDTH, False)


def display_history():
    text = []
    text.append('hjklyubn 12346789 or arrow keys - move')
    return menu('Message history', 'Messages so far:\n', text, DEFAULT_WINDOW_WIDTH, False)

def display_charinfo():
    text = wraplines(\
""" @ - this is you, a mere human adventurer.
    
    You are rather mundane.
    
    You do not possess any special abilities.
    
    You are on level 1 of the Dungeon.
    You are not hungry.
    
    You have visited 2 branches of the dungeon, and seen 7 of its levels.
    You have visited 1 portal chamber: sewer.
    
    You have collected 344 gold pieces.""",  
    (DEFAULT_WINDOW_WIDTH - 6))

    return menu('Character info', '', text, DEFAULT_WINDOW_WIDTH, False)

def wraplines(text, width):
    result = []
    for line in text.split('\n'):
        if line.strip() == "":
            result.append('')
        else:
            result += textwrap.wrap(line.strip(), width)
    return result

def sidebar_render(game):
    T.console_set_default_background(CON_BAR, T.black)
    T.console_clear(CON_BAR)
    render_bar(0,1,BAR_WIDTH,'HP',game.player.hp, game.player.max_hp, T.red, T.dark_red)
    T.console_print_ex(CON_BAR, 1, (BAR_HEIGHT - 2 ) , T.BKGND_NONE, T.LEFT, \
         "Turns: " + str(game.turns) + " ts(" + str(game.current_turn_speed) + ")" )

    T.console_print_ex(CON_BAR, 1, (BAR_HEIGHT - 3 ) , T.BKGND_NONE, T.LEFT, \
         "Level: " + str(game.level.name) )
