#!/bin/bash
echo "Cleaning up..."
rm -rf ./dist/*
./env/bin/python compile-static-dmap.py
./compile-extensions.sh
strip DijkstraMap.so
mkdir ./dist/linux-x64
mkdir ./dist/linux-x64/logs
mkdir ./dist/linux-x64/lib

cp *.py ./dist/linux-x64
cp DijkstraMap.so ./dist/linux-x64
cp libtcod.so ./dist/linux-x64
cp libSDL.so ./dist/linux-x64
cp DijkstraMap.so ./dist/linux-x64
cp DijkstraMap.so ./dist/linux-x64

cp -r ./lib/dill ./dist/linux-x64/lib
cp -r ./bestiary ./dist/linux-x64
cp -r ./dungeongen ./dist/linux-x64
cp -r ./fonts ./dist/linux-x64
cp -r ./save ./dist/linux-x64

cd ./dist && zip -r alchemyst.zip linux-x64
