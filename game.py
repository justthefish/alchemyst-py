import time, gc
from globals import *
import ui
import items
import move
import ai
import mob
from collections import deque
from logger import log
from level import Level
import tiles
import shelve
import bestiary.monsters as Mon
class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

#quit game
class Quit(Exception):
    pass

class Game(object):

    __metaclass__ = Singleton

    fpsticker = 0

    def __init__(self, wizard):
        self.turns = 0
        self.wizard = wizard
        self.player = None
        self.debug_ai = False


        # intentionally simplified speed system:
        # 1 - very slow (33% of normal)
        # 2 - slow (66% of normal)
        # 3 - normal (guess how much % of normal)
        # 4 - quick (133% of normal)
        # 5 - fast (166% of normal)
        # 6 - quick+fast or double (200% of normal)
        # each object having speed and some kind of ai performs a turn
        # in phase which speed is equal or lesser than theirs

        self.phases = deque([3,4,2,5,1,6])
        # self.phases = deque([1])

        self.current_turn_speed = 3
        #we start waiting for player
        self.wait_for_player_input = True
    
    def play(self):
        ui.init(self)
        mob.init(self)
        ui.title_screen()
        self.start()
        self.loop()
        ui.close()

    def start(self):

        self.level = Level(1)
        self.state = 'playing'

        self.player = Mon.Player()
        self.player.set_ai(ai.GenericAi())
        self.player.ai.take_turn = self.player_controls
        self.player.death_function = self.player_death

        #create current save
        data = shelve.open(SAVE_PATH+'data.p', 'n')
        data['player'] = 'player'
        data.close()

        self.level.objects.append(self.player)

        self.camera_x = 0
        self.camera_y = 0

        self.level.enter(self, 1)
        #(camera_x, camera_y) = (0,0)
        #self.fpsticker = 0


        #some silly text with objectives
        ui.message('Welcome, adventurer!', T.light_yellow)
        ui.message('''Get the orb of Xom out of Alchemysts Dungeon.''', T.light_yellow)

    def loop(self):
        log.debug('Stating game loop')
        while not T.console_is_window_closed():
            #rendering screen
            self.render_all()

            if self.checkGameState():
                break

            time.sleep(0.004)

    def checkGameState(self):
        #checking game states
        #here must be a switch!
        if self.state == 'dead':
            #do funky stuff like character dumping
            #ask player if he likes to try one more time
            ui.waitkey()
            return True

        elif self.state == 'win':
            #do funky stuff like character dumping
            #return to title
            pass
        elif self.state == 'title_screen':
            pass

        elif self.state == 'quit':
            #do funky stuff like character dumping
            return True
        else:
            #yay we are actually playing the game!
            self.update()



    def update(self):
        key = ui.readkey()
        if self.turns % 20 == 0:
            gc.collect()
        if not key:
            if self.state == 'autopilot':
                # self.wait_for_player_input = self.player_controls(key)
                self.process_phase()
                if self.player.speed >= self.current_turn_speed:
                    self.wait_for_player_input = self.player_controls(key)

            if not self.wait_for_player_input:
                self.process_phase()
                if self.player.speed >= self.current_turn_speed:
                    self.wait_for_player_input = True
            else:
                return
        if key:
            if self.state == 'autopilot':
                move.stop_autopilot(self)
                self.wait_for_player_input = True
                return
            if self.wait_for_player_input:
                self.wait_for_player_input = self.player_controls(key)

    def process_phase(self):
        for obj in self.level.objects:
            if isinstance(obj, Mon.Player):
                continue
            if isinstance(obj, mob.Mob) and obj.ai is not None and obj.speed >= self.current_turn_speed:
                obj.ai_map.recalc(obj.x, obj.y) #recalc
                obj.ai.take_turn() #do turn for all objects with enough speed

        self.phases.rotate(-1) #shifting deque left
        self.current_turn_speed = self.phases[0]
        self.heartbeat()
        self.turns += 1
        log.debug("=================<<< TURN " + str(self.turns) + " >>>================")


    #this is player ai :)
    #multi-turn actions are implemented with different game states
    def player_controls(self, key):
        self.player.ai_map.recalc(self.player.x, self.player.y) #recalc
        action_taken = False
        if self.state == 'autopilot':
            action_taken = move.autopilot(self)
        elif self.state == 'targeting':
            pass
        elif 'menu' in self.state:
            pass
        elif self.state == 'playing':
            self.player.waypoints = []
            action_taken = self.do_command(key)

        wait_for_player_input = not action_taken
        return wait_for_player_input

    def player_death(self):
        ui.message('You are sooo dead!', T.dark_red)
        #mob.Mob.generate_corpse(self.player, self.player)
        self.player.char='%'
        self.player.color = T.dark_red
        self.player.ai = None
        self.state = 'dead'

    def heartbeat(self):
        if self.current_turn_speed < 4: #normal speed or less
            for obj in self.level.objects:
                if isinstance(obj, mob.Mob):
                    obj.regen() #regenerate hp and mana
            #gc.collect() #manual garbage collection
            return


    #renderer
    def render_map(self):
        self.level.render(self)
        # item.render()
        mob.render()
        #map -> root
        T.console_blit(self.CON_MAP, 0,  0,  CAMERA_WIDTH, CAMERA_HEIGHT,  0,  20,         0)

    def render_other(self):
        if self.debug_ai:
                T.console_blit(self.CON_OVERLAY, 0,  0,  CAMERA_WIDTH, CAMERA_HEIGHT,  0,  20,         0, 1, 0.7)
        #log renger

        ui.sidebar_render(self)
        ui.log_render()
        #T.console_blit(GAME.CON_LOG, 0,  0,  80, (SCREEN_HEIGHT - CAMERA_HEIGHT), 0,  20, CAMERA_HEIGHT)
        T.console_blit(CON_BAR,0,0,BAR_WIDTH,BAR_HEIGHT, 0,0,0)

    def render_all(self):
        self.render_other()
        self.render_map()

        T.console_flush()


    #player commands

    def do_command(self, key):
        cmd = ui.decode_key(key)

        if cmd is None:
            return
        if isinstance(cmd, str):
            return getattr(self, 'cmd_'+cmd)()
        else:
            name, args = cmd
            return getattr(self, 'cmd_'+name)(*args)

    def cmd_walk(self, dx, dy):
        return self.player.walk(dx,dy)

    def cmd_wait(self):
        #we actually DO passing a turn, so return true
        return True

    def cmd_get(self):
        return True

    def cmd_drop(self):
        return True

    def cmd_inventory(self):
        return False

    def cmd_descend(self):
        if isinstance(self.level.map[self.player.x][self.player.y], tiles.DownStairTile):
            self.level.leave(self)
            self.level.enter(self, self.level.depth + 1)
            return True
        else:
            ui.message('There are no stairs here...', T.light_blue)
            return False

    def cmd_ascend(self):
        if isinstance(self.level.map[self.player.x][self.player.y], tiles.UpStairTile):
            if self.level.depth == 1:
                self.state = 'quit'
            else:
                self.level.leave(self)
                self.level.enter(self, self.level.depth - 1)
            return True
        else:
            ui.message('There are no stairs here...', T.light_blue)
            return False

    def cmd_look(self):
        return False

    def cmd_debug(self):
        for obj in self.level.objects:
            log.debug(str(obj.char) +" "+ str(obj.name) +" "+ str(obj.x) +" "+ str(obj.y) )
        return False

    def cmd_debug_toggle(self):
        self.debug_ai = not self.debug_ai
        return False

    def cmd_quit(self):
        #promt player if he is sure
        log.debug('quit command given!')
        self.state = 'quit'
        return False

    def cmd_help(self):
        ui.display_help();
        return False

    def cmd_history(self):
        ui.display_history();
        return False

    def cmd_charinfo(self):
        ui.display_charinfo();
        return False

    def cmd_cycle_font(self):
        log.debug('executing cycle font command')
        ui.cycle_font()
        return False

    def cmd_autopilot(self):
        ui.message("Autopilot engaged", T.grey)
        move.autopilot(self);
        return True

