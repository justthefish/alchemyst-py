import util
import dungeongen
from globals import MAP_WIDTH, MAP_HEIGHT, MAX_ROOMS, TORCH_RADIUS, FOV_LIGHT_WALLS, FOV_ALGO, FPS_LIMIT, \
    CAMERA_HEIGHT, CAMERA_WIDTH, SAVE_PATH
from logger import log
import libtcodpy as T
import shelve
import gc

class Level(object):
    """
    container for level data
    """
    name = None #Number of level
    depth = 1 #difficulty of level, int
    branch = None #branch to which it belongs
    max_rooms = MAX_ROOMS
    objects = []
    map_width = 0
    map_height = 0
    stairs = []
    room_centers = []

    def __init__(self, depth, branch="main", name=None):
        # print "init level ", depth, branch
        self.branch = branch
        self.depth = depth
        if name is None:
            self.name = branch.capitalize()[0]+":"+str(depth)
        else:
            self.name = name
        self.map_width = MAP_WIDTH
        self.map_height = MAP_HEIGHT
        self.map = [[None for y in xrange(0, self.map_height +1 )] for x in xrange(0, self.map_width + 1)]
        self.objects = []
        self.stairs = []
        self.room_centers = []

    def get_genstack(self, branch, depth):
        stack = [dungeongen.new.Gen]
        # @todo rewrite river
        # if util.random_int(0,5) > 4:
        #     stack.append(dungeongen.river)
        return stack

    def generate_map(self):
        log.debug('Generating map for dungeon '+ self.name +" (" + str(self.branch) +", depth "+ str(self.depth)+")")
        stack = self.get_genstack(self.branch, self.depth)
        for gen_name in stack:
            gen_instance = gen_name(self.map_width, self.map_height, self.depth)
            self.room_centers, self.spawns, self.stairs = gen_instance.add_rooms(self.max_rooms, self.stairs)
            self.stairs = gen_instance.add_stairs(self.stairs)
            self.map = gen_instance.draw_rooms(self.map, self.branch, self.depth)
            self.objects += gen_instance.itemize(self.branch, self.depth)
            self.objects += gen_instance.populate(self.branch, self.depth)
            gen_instance = None

    def enter(self, game, depth):
        gc.collect()
        self.__init__(depth)
        # shamanism, because pickle cannot save cython objects
        game.player.ai_map.restore()
        # print repr(game.player.ai_map)

        if self.load():
            game.player.x, game.player.y = self.room_centers[util.random_int(0, (len(self.room_centers) -1))]
        else:
            for obj in self.objects:
                self.objects.remove(obj)
            self.generate_map()
            game.player.x, game.player.y = self.room_centers[util.random_int(0, (len(self.room_centers) -1))]
        self.fov_init()
        game.fov_recompute = True
        self.objects.append(game.player)

    def leave(self, game):
        # shamanism, because pickle cannot save cython objects
        game.player.ai_map.reset() # not pre_save, because player has hno AI
        self.objects.remove(game.player)
        self.save()

    def load(self):
        result = False
        save = shelve.open(SAVE_PATH+'data.p', 'r')
        data = util.get_shelve_key(save, self.name)
        save.close()

        if data:
            objs = data["objects"]
            # shamanism, because pickle cannot save cython objects
            for obj in objs:
                if "post_load" in dir(obj):
                    obj.post_load()
            self.map = data['map']
            self.room_centers = data['room_centers']
            self.objects = objs
            result = True
        return result

    def save(self):
        #prepare objects
        for obj in self.objects:
            if "pre_save" in dir(obj):
                obj.pre_save()
        data = shelve.open(SAVE_PATH+'data.p', 'c')
        data[self.name] = {
            "map": self.map,
            "objects": self.objects,
            "room_centers": self.room_centers
        }
        data.close()

    def render(self, game):
        redraw = False
        self.move_camera(game.player.x, game.player.y, game)
        if game.fov_recompute:
            T.console_clear(game.CON_MAP)
            T.console_clear(game.CON_OVERLAY)
            game.fov_recompute = False
            redraw = True
            T.map_compute_fov(self.fov_map, game.player.x, game.player.y, TORCH_RADIUS, FOV_LIGHT_WALLS, FOV_ALGO)

        game.fpsticker +=1
        if redraw == True or (game.fpsticker % (FPS_LIMIT // 10)) == 0:
            game.fpsticker = 0
            #redraw once a second

            for y in range (CAMERA_HEIGHT):
                for x in range (CAMERA_WIDTH):
                    (map_x, map_y) = (game.camera_x + x, game.camera_y +y)

                    tile = self.map[map_x][map_y]
                    visible = T.map_is_in_fov(self.fov_map, map_x, map_y)

                    if game.debug_ai:
                        # log.debug(str(["{}".format(i) for i in game.player.ai_map.get_dmap().dmap]))
                        try:
                            ai_val = game.player.ai_map.get_dmap().dmap[map_x][map_y]
                            ai_val = ai_val // 120
                            if ai_val > 255:
                                ai_val = 255
                            ai_val = 255 - (15 * ai_val)
                            T.console_put_char_ex(game.CON_OVERLAY, x, y, " ", tile.fg(), T.Color(ai_val, ai_val, ai_val))
                        except:
                            T.console_put_char_ex(game.CON_OVERLAY, x, y, " ", T.black, T.black)
                            pass
                    if not visible:
                       if tile.must_draw:
                           T.console_put_char_ex(game.CON_MAP,x,y, tile.char, tile.dark_fg(), tile.dark_bg())
                    else:
                       if redraw == True or tile.colordance == True:
                           T.console_put_char_ex(game.CON_MAP, x, y, tile.char, tile.fg(), tile.bg())
                           #print gas to its own console
                           tile.explored = True
                           tile.must_draw = True

    def move_camera(self, target_x, target_y, game):
        #global camera_x, camera_y, fov_recompute
        #new camera coordinates (top-left corner of the screen relative to the map)
        x = target_x - CAMERA_WIDTH / 2  #coordinates so that the target is at the center of the screen
        y = target_y - CAMERA_HEIGHT / 2
        #make sure the camera doesn't see outside the map
        if x < 0: x = 0
        if y < 0: y = 0
        if x > MAP_WIDTH - CAMERA_WIDTH - 1: x = MAP_WIDTH - CAMERA_WIDTH - 1
        if y > MAP_HEIGHT - CAMERA_HEIGHT - 1: y = MAP_HEIGHT - CAMERA_HEIGHT - 1
        if x != game.camera_x or y != game.camera_y: game.fov_recompute = True
        (game.camera_x, game.camera_y) = (x, y)


    def fov_init(self):
        self.fov_map = T.map_new(MAP_WIDTH, MAP_HEIGHT)
        for y in range(MAP_HEIGHT):
            for x in range(MAP_WIDTH):
                tile = self.map[x][y]
                T.map_set_properties(self.fov_map, x, y, not tile.block_sight, not tile.block_pass)
