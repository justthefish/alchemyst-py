import libtcodpy as T

def random_int(min, max):
    return T.random_get_int(0, min, max)

def dice(string):
    num, s_bias = string.split("d")
    sides, bias = s_bias.split("+")
    result = 0
    for i in range(int(num)):
        result = result + T.random_get_int(0, 1, int(sides))
    result = result + int(bias)
    return result

def random_choice_index(chances):  #choose one option from list of chances, returning its index
    #the dice will land on some number between 1 and the sum of the chances
    dice = T.random_get_int(0, 1, sum(chances))

    #go through all chances, keeping the sum so far
    running_sum = 0
    choice = 0
    for w in chances:
        running_sum += w
        #see if the dice landed in the part that corresponds to this choice
        if dice <= running_sum:
            return choice
        choice += 1

def random_choice(chances_dict):
    #choose one option from dictionary of chances, returning its key
    chances = chances_dict.values()
    strings = chances_dict.keys()
    return strings[random_choice_index(chances)]

def get_shelve_key(shelve, key):
    try:
        result = shelve[key]
    except Exception:
        result = False
    return result