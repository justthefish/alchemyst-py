from distutils.core import setup
from Cython.Build import cythonize
import numpy

setup(
    name="Alchemyst",
    ext_modules =cythonize(["*.pyx", "move/*.pyx"]),
    include_dirs=[numpy.get_include()]
)
